const mongoose = require('mongoose');
const orderSchema = new mongoose.Schema({
	products: [
		{
			productId: {
				type: Number,
				required: [true, 'Product Id is required']
			},
			quantitiy: {
				type: Number,
				required: [true, 'Product quantity is required']
			}
		}
	],
	totalAmount: {
		type: Number,
		required: [true, 'Total Amount is required']
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	}
});

export.module = mongoose.model('Order', orderSchema);