const User = require('../models/user.js');
const Course = require('../models/course.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js')

module.exports.checkEmailExist = (reqBody) => {

	// '.find' - a mongoose crud operation (query) to find a field value from a collection
	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0) {
			return true;
		}
		else
		{
			return false;
		}
	})
}

module.exports.registerUser = (reqBody) => {
	let newUser = new User({	
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10),
/*
		// bcrypt - package for password hashing
		// hash - asynchronously generates a hash
		// hasSync - synchronously generates a hash
		// 10 = salt rounds
		// salt rounds is proportional to hashing rounds, the higher the salt rounds, the more hashing rounds, the longer it takes to generate an output
*/
		mobileNo: reqBody.mobileNo,
	})

	return newUser.save().then((user, error) => {
		if(error) {
			return false;
		}
		else{
			return true;
		}

	})
}

module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null) {
			return false;
		}
		else {
			// compareSync is bcrypt function to compare an unhashed password to hashed password
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			
			if(isPasswordCorrect) {
				return {access: auth.createAccessToken(result)};
			}
			else {
				// if password does not match
				return false;
				// return "Incorrect password"
			}
		}
	});
}

// S38 activity
module.exports.getProfile = (reqBody) => {
	return User.findOne({_id: reqBody.id}).then(result => {
		result.password = '**********';
		return result;
	})
}

/*
	1. Create a "/details" or "details/:id" route that will accept the user’s Id to retrieve the details of a user.
	2. Create a getProfile controller method for retrieving the details of the user:
	        - Find the document in the database using the user's ID
	        - Reassign the password of the returned document to an empty string ("") / ("*****")
	        - Return the result back to the postman
	 3. Process a GET or POST request at the /details/:id route using postman to retrieve the details of the user.
	 4. Updated your s37-41 remote link, push to git with the commit message of "Add activity code - S38".
	 5. Add the link in Boodle.
*/

// Enroll user to a class

module.exports.enroll = async(data) => {

	let isUserUpdated = await User.findById(data.user).then(user => {

		user.enrollments.push({courseId: data.courseId});
		return user.save().then((user, error) => {
			if(error) {
				return false;
			} else {
				return true;
			}
		});
	});

	let isCourseUpdated = await Course.findById(data.courseId).then(course => {

		course.enrollees.push({userId: data.userId});
		return course.save().then((course, error) => {
			if (error) {
				return false;
			} else {
				return true;
			}
		});
	});


	if(isUserUpdated && isCourseUpdated) {
		return true;
	} else {
 		return false;
	};

};