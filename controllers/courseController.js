const Course = require("../models/course.js");


// module.exports.addCourse = (reqBody) => {
	
// 	let newCourse = new Course ({
// 		name: reqBody.name,
// 		description: reqBody.description,
// 		price: reqBody.price
// 	})

// 	return newCourse.save().then((newCourse, error) => {
// 		if(error) {
// 			return error;
// 		}
// 		else {
// 			return newCourse;
// 		}
// 	})
// }



module.exports.getAllCourse = () => {
	return Course.find({}).then(result => result)
}

module.exports.getActiveCourses = () => {
	return Course.find({isActive: true}).then(result => result)
}

// GET specific course
module.exports.getCourse = (courseId) => {
					//inside the parenthesis should be the id
	return Course.findById(courseId).then(result => result)
}

module.exports.updateCourse = (courseId, newData) => {
	if(newData.isAdmin == true) {
		return Course.findByIdAndUpdate(courseId, 
			{
				name: newData.course.name,
				description: newData.course.description,
				price: newData.course.price
			}
		).then((updateCourse, error) => {
			if (error) {
				return false;
			}
			return true;
		})

	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this');
		return message.then(value => value)
	}
}

// ========================================================
// Activity
// Refactor the addCourse controller method to implement admin authentication for creating a course

module.exports.addCourse = (createCourse) => {
	if(createCourse.isAdmin == true ){
		let newCourse = new Course ({
			name: createCourse.course.name,
			description: createCourse.course.description,
			price: createCourse.course.price
		})

		return newCourse.save().then((newCourse, error) => {
			if(error) {
				return error;
			}
			else {
				return newCourse;
			}
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this');
		return message.then(value => value)
	}
	
}

module.exports.archiveCourse = (courseId, data) => {
	if(data.isAdmin == true) {
		return Course.findByIdAndUpdate(courseId, 
			{
				// isActive: data.course.isActive
				isActive: false
			}
		).then((archivedCourse, err)=> {
			if(err) {
				console.log(err);
				return false;
			}
			else {
				// return true;
				return "message: Course successfully archived!"
			}
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this');
		return message.then(value => value)
	}
}
// Create a controller method for archiving a course obtaining the course ID from the request params and the course information from the request body




// ==================================================