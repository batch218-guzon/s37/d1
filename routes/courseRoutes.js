const express = require('express'); // accessing package of express
const router = express.Router(); // use dot notation to access content of a package

const courseController = require('../controllers/courseController.js');
const auth = require("../auth.js");

// Create a single course
// router.post("/create", (req, res) => {
// 	courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController))
// });

router.get("/all", (req, res) => {
	courseController.getAllCourse().then(resultFromController => res.send(resultFromController))
})

router.get("/active", (req, res) => {
	courseController.getActiveCourses(req.body).then(resultFromController => res.send(resultFromController))
})

router.get("/:courseId", (req, res) => {
								//retrieves the id from the url
	courseController.getCourse(req.params.courseId).then(resultFromController => res.send(resultFromController))
})

router.patch("/:courseId/update", auth.verify, (req, res) => {
	const newData = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	courseController.updateCourse(req.params.courseId, newData).then(resultFromController => res.send(resultFromController))
})

// ==================================================================================================

// Activity
// Refactor the course route to implement user authentication for the admin when creating a course


router.post("/create", auth.verify, (req, res) => {
	const createCourse = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	courseController.addCourse(createCourse).then(resultFromController => res.send(resultFromController))
});

// Create a route for archiving a course. The route must use JWT authentication and obtain the course ID from the url

router.patch("/:courseId/archive", auth.verify, (req, res) => {
	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	courseController.archiveCourse(req.params.courseId, data).then(resultFromController => res.send(resultFromController))
})



module.exports = router;