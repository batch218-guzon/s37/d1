const express = require('express');
const router = express.Router();
const User = require('../models/user.js');
const userController = require('../controllers/userController.js');
const auth = require("../auth.js");

router.post('/checkEmail', (req, res) => {
	userController.checkEmailExist(req.body).then(resultFromController => res.send(resultFromController))
});

router.post('/register', (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

router.post('/login', (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});

// S38 activity

router.post('/details', (req, res) => {
	userController.getProfile(req.body).then(resultFromController => res.send(resultFromController))
});


// Activity
// Refactor the user route to implement user authenctication for the enroll route
// Process a POST request at the /enroll route using postman to enroll a user to a course

router.post('/enroll', auth.verify, (req, res) => {
	const data = {
		courseId: req.body.courseId,
		userId: req.body.userId,
		user: auth.decode(req.headers.authorization).id
		// userId: auth.decode(req.headers.authorization).id
	}
	userController.enroll(data).then(resultFromController => res.send(resultFromController))
})

module.exports = router;
