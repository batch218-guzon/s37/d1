/*
	Booking System API -
	MVP -  Minimum Viable Product

	 Our Booking System MVP must have the following features:
		-user registration
		-user authentication (login)
		-retrieval of authenticated user's details
		-course creation by an authenticated user
		-retrieval of courses
		-course info updating by an authenticated user
		-course archiving by an authenticated user

	Booking System API Dependencies
			-npm init -y
		1. express
			-npm i express
		2. mongoose
			-npm i mongoose
		3. bcrypt
			-hashing user passwords prior to storage
			-npm i bcrypt
		4. cors
			-for allowing cross-origin resourse sharing
			-https://www.educative.io/answers/how-cors-cross-origin-resource-sharing-works
			-npm install cors
			-npm i cors
		5. jsonwebtoken
			-for implementing JSON web tokens
			-npm i jsonwebtoken
*/

const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

const userRoutes = require('./routes/userRoute.js')
const courseRoute = require('./routes/courseRoutes.js')

const app = express();


// Middlewares - allows to bridge our backend applicaton (server) to our front end
// to allow cross-origin resource sharing
app.use(cors());
// to read json objects
app.use(express.json());
// to read forms
app.use(express.urlencoded({extended:true}));

app.use("/users", userRoutes);
app.use("/courses", courseRoute);

// Connect to our MongoDB Database
mongoose.connect('mongodb+srv://admin:admin@batch218-coursebooking.3fcsn94.mongodb.net/courseBooking?retryWrites=true&w=majority', {
	useNewUrlParser:true,
	useUnifiedTopology: true
});

// prompts a message once connected
mongoose.connection.once('open', () => console.log('Now connected to Guzon-Mongo DB Atlas.'));

// prompts a message once connected to port 4000
// 3000, 4000, 5000, 8000 - port numbers for web applications
app.listen(process.env.PORT || 4000, () => {console.log(`API is now online on port ${process.env.PORT || 4000}`)});