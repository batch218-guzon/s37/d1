const jwt = require('jsonwebtoken');
const secret = 'CourseBookingApi';

module.exports.createAccessToken = (user) => {
	// payload
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}
								// callback function // {expiresIn : '60s'}
	return jwt.sign(data, secret, {});
};

// to verify a token from the request (from postman)
module.exports.verify = (request, response, next) => {

	// Get JWT (JSON web Token) from postman
	let token = request.headers.authorization

	if(typeof token !== "undefined") {
		console.log(token);

		// remove first 7 characters ('Bearer ') from the token
		token = token.slice(7, token.length);
		
/*		
	parameters
		1.jwtSting
		2.secretOrPublicKey
		3.callback
	https://github.com/auth0/node-jsonwebtoken/blob/master/verify.js
*/

		return jwt.verify(token, secret, (error, data) => {
			if(error) {
				return response.send({auth: "Failed."})
			}
			else {
				next()
			}
		})
	}
	else{
		return null;
	}
}

// to decode the user details from the token
module.exports.decode = (token) => {
	if(typeof token !== "undefined") {
		token = token.slice(7, token.length);
	}

	return jwt.verify(token, secret, (error, data) => {
		if(error) {
			return null
		}
		else {
			return jwt.decode(token, {complete:true}).payload
		}
	})
}

// jwt part
// header(alg,type)      payload     signature
// abcdxqwertyuiadsfads.qwerujkasdl.poaaweamnan